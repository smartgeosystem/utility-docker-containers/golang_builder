# GoLang Builder

Builder for GoLang projects.

Ubuntu 20.04 based:
- GoLang 1.13
- GoLang 1.16

Ubuntu 22.04 based:
- GoLang 1.17
- GoLang 1.18

Packages:

- GoLang
- git
- build-essential
- ca-certificates
- make
- curl